from core.data_types.image_library import ImageLibrary
from tests.consts import IMAGE_FILENAMES
from tests.utils import create_image_library, create_image


def test_find_most_common_face():
    image_library = create_image_library()
    most_common_face = image_library.find_most_common_face()
    assert isinstance(most_common_face, list)
    assert len(most_common_face) == 3


def test_find_common_face_empty_lib():
    image_library = ImageLibrary(images=[])
    most_common_face = image_library.find_most_common_face()
    assert most_common_face == []


def test_find_common_face_no_dup_faces():
    """
    test the case when each face exist exactly once.
    the function should return the first face id from the library.
    """
    image_library = ImageLibrary(images=[create_image(IMAGE_FILENAMES[0]), create_image(IMAGE_FILENAMES[1])])
    most_common_face = image_library.find_most_common_face()
    assert isinstance(most_common_face, list)
    assert len(most_common_face) == 1


def test_find_common_face_with_one_face():
    image_library = ImageLibrary(images=[create_image(IMAGE_FILENAMES[0])])
    most_common_face = image_library.find_most_common_face()
    assert most_common_face == []


def test_constructor():
    image_library = create_image_library()
    assert isinstance(image_library, ImageLibrary)
    assert isinstance(image_library.images, list)
    assert len(image_library.images) == 4

    assert isinstance(image_library.face_ids, list)
    assert len(image_library.face_ids) == 6

    assert isinstance(image_library.face_id_to_image, dict)
    assert len(image_library.face_id_to_image) == 6
