from tests.utils import create_image


def test_image_size():
    image = create_image()
    width, height = image.get_image_size()
    assert width == 1536
    assert height == 2048
