from tests.utils import create_best_image_response


def test_best_image_response():
    best_image_response = create_best_image_response()
    assert best_image_response.__dict__() == {
        'FaceMetaData': {'width': 81, 'top': 799, 'height': 81, 'additional_properties': {}, 'left': 771},
        'FileName': '1.jpg'}
