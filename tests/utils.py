import os.path

from core.data_types.image import Image
from core.data_types.image_library import ImageLibrary
from core.data_types.best_image_response import BestImageResponse
from configuration import IMAGES_DIR_PATH
from tests.consts import IMAGE_FILENAMES

DEFAULT_IMAGE_NAME = IMAGE_FILENAMES[0]


def create_image(image_name=DEFAULT_IMAGE_NAME):
    file_path = os.path.join(IMAGES_DIR_PATH, image_name)
    with open(file_path, 'rb') as f:
        image = Image(file_path=file_path, content=f.read())

    return image


def create_image_library():
    images = [create_image(image_filename) for image_filename in IMAGE_FILENAMES]
    return ImageLibrary(images=images)


def create_best_image_response():
    image = create_image()
    return BestImageResponse(image=image, face_id=image.info[0].face_id)
