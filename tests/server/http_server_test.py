from core.server.http_server import FindImage, UNSUPPORTED_MESSAGE_TO_CLIENT
from tests.consts import IMAGE_FILENAMES_STR


def test_sanity():
    find_img_service = FindImage()
    response = find_img_service.get(image_filenames=str(IMAGE_FILENAMES_STR))
    assert response == {'FaceMetaData': {'width': 174, 'top': 108, 'height': 174,
                                         'additional_properties': {}, 'left': 601}, 'FileName': '2.jpg'}


def test_not_json():
    find_img_service = FindImage()
    response = find_img_service.get(image_filenames='a')
    assert response == UNSUPPORTED_MESSAGE_TO_CLIENT


def test_not_list():
    find_img_service = FindImage()
    response = find_img_service.get('{"1":"2"}')
    assert response == UNSUPPORTED_MESSAGE_TO_CLIENT


def test_empty_input():
    find_img_service = FindImage()
    response = find_img_service.get('')
    assert response == UNSUPPORTED_MESSAGE_TO_CLIENT
