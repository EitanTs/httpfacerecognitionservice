from tests.utils import create_image

from core.api.image_api import find_best_image, _get_image_library, _rate_image_quality
from core.data_types.best_image_response import BestImageResponse
from core.data_types.image import Image
from core.data_types.image_library import ImageLibrary

image_filenames = ["2.jpg", "3.jpg"]


def test_find_best_image():
    best_image = find_best_image(image_filenames)
    image_info = best_image.image.info

    assert isinstance(best_image, BestImageResponse)
    assert best_image.face_id

    assert image_info[0].face_rectangle.__dict__ == {'width': 177, 'top': 134, 'height': 177,
                                                     'additional_properties': {}, 'left': 133}
    assert image_info[1].face_rectangle.__dict__ == {'width': 174, 'top': 108, 'height': 174,
                                                     'additional_properties': {}, 'left': 601}
    assert image_info[2].face_rectangle.__dict__ == {'width': 135, 'top': 182, 'height': 135,
                                                     'additional_properties': {}, 'left': 382}

    assert best_image.__dict__() == {'FaceMetaData': {'width': 174, 'top': 108, 'height': 174,
                                                      'additional_properties': {}, 'left': 601}, 'FileName': '2.jpg'}


def test_rate_image_quality():
    image = create_image()
    image_quality = _rate_image_quality(image, image.info[0].face_id)
    assert image_quality == 0.0020856857299804688


def test_get_image_library():
    image_library = _get_image_library(image_filenames)
    assert isinstance(image_library, ImageLibrary)

    assert len(image_library.images) == 2
    for image in image_library.images:
        assert isinstance(image, Image)

    assert len(image_library.face_ids) == 4
    for face_id in image_library.face_ids:
        assert isinstance(face_id, unicode)

    for image in image_library.images:
        for detected_face in image.info:
            assert detected_face.face_id in image_library.face_ids


def test_all_files_not_exist():
    best_image = find_best_image('1')

    assert isinstance(best_image, BestImageResponse)
    assert best_image.image is None
    assert best_image.face_id is None
    assert best_image.__dict__() == {"message": "no image detected"}


def test_one_file_not_exist():
    best_image = find_best_image(['1', '2.jpg'])

    assert isinstance(best_image, BestImageResponse)
    assert best_image.__dict__() == {'FaceMetaData': {'width': 177, 'top': 134, 'height': 177,
                                                      'additional_properties': {}, 'left': 133}, 'FileName': '2.jpg'}


def test_empty_input():
    best_image = find_best_image('')

    assert isinstance(best_image, BestImageResponse)
    assert best_image.__dict__() == {"message": "no image detected"}
