import os.path
import logging

from configuration import IMAGES_DIR_PATH
from core.data_types.best_image_response import BestImageResponse
from core.data_types.image import Image
from core.data_types.image_library import ImageLibrary


def find_best_image(image_filenames):
    """

    :param image_filenames: list of local image file names
    :return: Instance of BestImageResponse class, which contains instance of the best image,
    and the face id in this image which is the most common face from all images.
    """
    best_image = None
    best_face_id = None
    highest_quality = 0

    image_library = _get_image_library(image_filenames)
    most_common_face_images = image_library.find_most_common_face()
    for face_id in most_common_face_images:
        image = image_library.face_id_to_image[face_id]
        image_quality = _rate_image_quality(image, face_id)

        if image_quality > highest_quality:
            best_image = image
            best_face_id = face_id

    return BestImageResponse(image=best_image, face_id=best_face_id)


def _rate_image_quality(image, face_id):
    """

    :param image: instance of Image class
    :param face_id: unique face id
    :return: float that calculated by the relation between face rectangle and image size.
    """
    face_height = 0.0
    face_width = 0.0

    for detected_face in image.info:
        if detected_face.face_id == face_id:
            face_height = detected_face.face_rectangle.height
            face_width = detected_face.face_rectangle.width
            break

    return (face_height * face_width) / float(image.image_height * image.image_width)


def _get_image_library(image_filenames):
    """

    :param image_filenames: list of local image file names
    :return: instance of ImageLibrary class
    """
    images = []

    for image_filename in image_filenames:
        if not isinstance(image_filename, str):
            image_filename = str(image_filename)

        image_path = os.path.join(IMAGES_DIR_PATH, image_filename)
        try:
            image_data = _get_file_data(image_path)
        except IOError:
            logging.warning('no such file or directory ---- {0}'.format(image_path))
            continue

        image = Image(file_path=image_path, content=image_data)
        images.append(image)

    image_library = ImageLibrary(images=images)
    return image_library


def _get_file_data(file_path):
    with open(file_path, 'rb') as f:
        return f.read()
