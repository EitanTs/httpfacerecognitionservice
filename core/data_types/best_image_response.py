import ntpath

from core.data_types.image import Image


class BestImageResponse(object):
    def __init__(self, image, face_id):
        self.image = image
        self.face_id = face_id

    def __dict__(self):
        """
        :return: Well formatted dict with face info, and image file name.
        """
        if not isinstance(self.image, Image):
            return {"message": "no image detected"}

        face_metadata = None

        for detected_face in self.image.info:
            if detected_face.face_id == self.face_id:
                face_metadata = detected_face.face_rectangle.__dict__

        return {'FileName': ntpath.basename(self.image.file_path), 'FaceInfo': face_metadata}
