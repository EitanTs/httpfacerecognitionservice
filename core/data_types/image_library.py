from copy import deepcopy

from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials

from core.api.api_configuration import FACE_API_URL, SUBSCRIPTION_KEY

MINIMUM_FACES_TO_COMPARE = 2


class ImageLibrary(object):
    """
        This class represents "library of our images"
        The library contains -
            - list of images objects
            - list of all faces ids detected from the images
            - dict which relating between any face id to it's image object
    """

    def __init__(self, images):
        self.__images = images
        self.face_ids = []
        self.face_id_to_image = {}
        self.face_client = FaceClient(FACE_API_URL, CognitiveServicesCredentials(SUBSCRIPTION_KEY))

        self._set_params()

    def find_most_common_face(self):
        """
        :return: list of face ids, the face ids are belong to the face that appeared the most times.
        """
        if len(self.face_ids) < MINIMUM_FACES_TO_COMPARE:
            return []

        most_common_face_ids = []

        for face_id in self.face_ids:
            copied_face_ids = deepcopy(self.face_ids)
            copied_face_ids.remove(face_id)  # in order to avoid comparing face to it self
            similar_faces = self.face_client.face.find_similar(face_id=face_id, face_ids=copied_face_ids)

            if len(similar_faces) > len(most_common_face_ids):
                most_common_face_ids = [similar_face.face_id for similar_face in similar_faces]
                most_common_face_ids.append(face_id)

        if not most_common_face_ids:
            return [self.face_ids[0]]  # there are no duplicate faces in the photos, so we send the first one.

        return most_common_face_ids

    def _set_params(self):
        for image in self.__images:
            face_ids = image.face_ids
            self.face_ids.extend(face_ids)

            for face_id in face_ids:
                self.face_id_to_image[face_id] = image

    @property
    def images(self):
        return self.__images
