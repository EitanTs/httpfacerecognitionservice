import io

from PIL import Image as PILImage
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials

from core.api.api_configuration import FACE_API_URL, SUBSCRIPTION_KEY


class Image(object):
    def __init__(self, file_path, content):
        self.face_client = FaceClient(FACE_API_URL, CognitiveServicesCredentials(SUBSCRIPTION_KEY))
        self.file_path = file_path
        self.content = io.BytesIO(content)

        self.image_width, self.image_height = self.get_image_size()
        self.info = self._get_image_info_from_azure()
        self.face_ids = [detected_face.face_id for detected_face in self.info]

    def _get_image_info_from_azure(self):
        return self.face_client.face.detect_with_stream(image=self.content)

    def get_image_size(self):
        """
        :return: tuple of image height and width
        """
        im = PILImage.open(self.file_path)
        return im.size
