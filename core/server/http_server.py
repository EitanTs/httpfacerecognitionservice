import logging
import json

from flask import Flask
from flask_restful import Resource, Api

import core.api.image_api
from configuration import PORT

app = Flask(__name__)
api = Api(app)

UNSUPPORTED_MESSAGE_TO_CLIENT = 'Unsupported format, please enter a valid json formatted as a list'
UNSUPPORTED_LOG_FORMAT = 'unsupported format received ---- {0}'


class Index(Resource):
    """
        explain to the clients using method of our service
    """

    def get(self):
        return '''Please go to -> /find_image/<image_filenames> Example: http://127.0.0.1:5002/find_image/[{image file names}]'''


class FindImage(Resource):
    """
        treat get requests to this url - http://127.0.0.1:5002/find_image/["image_name", "image_name_2"...]

        input: list formatted string
        output: json with image metadata from azure api and image file name.
        The image which sent is "the best image" and the one with the most common face detected from all the images.
    """

    def get(self, image_filenames):
        try:
            image_filenames = json.loads(image_filenames)
        except ValueError:
            logging.warning(UNSUPPORTED_LOG_FORMAT.format(image_filenames))
            return UNSUPPORTED_MESSAGE_TO_CLIENT

        if not isinstance(image_filenames, list):
            logging.warning(UNSUPPORTED_LOG_FORMAT.format(image_filenames))
            return UNSUPPORTED_MESSAGE_TO_CLIENT

        best_image = core.api.image_api.find_best_image(image_filenames)
        return best_image.__dict__()


api.add_resource(Index, '/')  # Route
api.add_resource(FindImage, '/find_image/<image_filenames>')  # Route

if __name__ == '__main__':
    app.run(port=PORT)
