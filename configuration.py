import os.path

IMAGES_DIR_NAME = 'images'
IMAGES_DIR_PATH = os.path.join(os.path.dirname(__file__), IMAGES_DIR_NAME)

PORT = '5002'

